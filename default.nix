with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "talk-template";
  src = ./.;
  buildInputs = [
    gnumake
    pandoc
    texlive.combined.scheme-small
  ];
  installPhase = ''
    mkdir -p $out
    cp -R files index.html index.pdf $out
  '';
}

