all: index.html index.pdf

index.html: index.md
	pandoc index.md -o index.html -s -i -t revealjs -V revealjs-url=files/local/reveal.js-4.4.0 -V theme=white -V transition=fade -V slideNumber=true -V fragments=false -V center=true -V history=false --css files/my-revealjs.css --css files/local/syntax/pygments.css --katex=files/local/katex/ --section-divs --slide-level=2
	mkdir -p files/local
	find archives -maxdepth 1 -name "*.tar.gz" -exec tar zxf {} -C files/local \;

index.pdf: index.md
	pandoc index.md -t beamer -o index.pdf -H custom/my-beamer-header.sty --section-divs --slide-level=2

clean:
	rm -f index.html index.pdf

