
# talk-template

## description

- input: markdown
- output: revealjs + pdf

## usage

- install pandoc and latex
- run `make`

## usage with Nix

```
$ nix-shell
[nix-shell]$ make
```

## references

- [slides](http://juliendehos.gitlab.io/talk-template/index.html)
- [pdf](http://juliendehos.gitlab.io/talk-template/index.pdf)
- [source code](https://gitlab.com/juliendehos/talk-template)

