---
title: My talk template
subtitle: International Conference on Talk Templates
date: 2019
author: Author One, Author Two
---


# Section 1 

## Lists

- foo
- bar
- foobar
- barfoo

## Code

```nix
with import <nixpkgs> {};
stdenv.mkDerivation rec {
  name = "talk-template";
  src = ./.;
  buildInputs = [
    gnumake
    pandoc
  ];
  installPhase = ''
    mkdir -p $out
    cp -R files local index.html index.pdf $out
  '';
}
```


# Section 2 

## Maths

$$F^{42}(n) = 
\left\{
    \begin{array}{l}
        n \text{ si } n \in \{0, 1\} \\
        F^{42}(n-1) + F^{42}(n-2) \mod 42 \text{ sinon }
    \end{array}
    \right.$$

## Images

![](files/spongebob.png){width="20%"}


# Conclusion


## References

- <https://nixos.org>

--------------------------------------------------

Thank you !

Questions & comments ?

--------------------------------------------------


